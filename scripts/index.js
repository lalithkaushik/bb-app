var url="http://lalithkaushik.com/mayot_admin_new/services/";
//var url="http://localhost/mayot_new_theme/public_html/services/";

$(window).load(function() { // makes sure the whole site is loaded
		$("#status").fadeOut(); // will first fade out the loading animation
		$("#preloader").delay(350).fadeOut("slow"); // will fade out the white DIV that covers the website.
		$(".content").show();

});

function current_unix_timestamp(){
	
	var ts = Math.round((new Date()).getTime() / 1000);
	return ts;
}


function login()
{

	
	var flag=0;
	var email_mobile=$('#email_mobile').val();
	var pwd=$('#pwd').val();
	
	if(email_mobile=='')
	{
		flag=1;
		$('#email_mobile').css('border-color','#F00');
	}
	else
	{
		flag=0;
		$('#email_mobile').css('border-color','#CCC');
	}
	
	if(pwd=='')
	{
		flag=1;
		$('#pwd').css('border-color','#F00');
	}
	else
	{
		flag=0;
		$('#pwd').css('border-color','#CCC');
	}
	
	
	if(flag==1)
	{
		alert("Please enter the highlighted fields");
	}
	else
	{
		$('#validating_div div').html('Validating...');
		

		var network = navigator.connection.type;
		network = ""+network;
		if(network=="none"){
			local_validation(email_mobile, pwd);
		}
		else{
			server_validation(email_mobile, pwd);
		}

	}
}

function checkConnection(){

    var network = navigator.connection.type;

    var states = {};
    states[Connection.UNKNOWN]  = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI]     = 'WiFi connection';
    states[Connection.CELL_2G]  = 'Cell 2G connection';
    states[Connection.CELL_3G]  = 'Cell 3G connection';
    states[Connection.CELL_4G]  = 'Cell 4G connection';
    states[Connection.NONE]     = 'No network connection';

    alert('Connection type: ' + states[network]);
}


function server_validation(email_mobile, pwd){
	$.getJSON(url+"verify.php?email_mobile="+email_mobile+"&pwd="+pwd ,function(response){
		var yp_info = response;

		if(yp_info.yp_id !="0"){
			
			window.localStorage.setItem("member_id",yp_info.yp_id);
			window.localStorage.setItem("yp_name",yp_info.yp_name);
			window.localStorage.setItem("yp_email",yp_info.yp_email);
			window.localStorage.setItem("yp_mobile",yp_info.yp_mobile);
			window.localStorage.setItem("yp_dob",yp_info.yp_dob);
			window.localStorage.setItem("yot_id",yp_info.yot_id);
			window.localStorage.setItem("yp_start_date",yp_info.yp_start_date);
			window.localStorage.setItem("yp_end_date",yp_info.yp_end_date);
			
			window.localStorage.setItem("yw_name",yp_info.yw_name);
			window.localStorage.setItem("yw_mobile",yp_info.yw_mobile);
			window.localStorage.setItem("yw_no_type",yp_info.yw_no_type);
			window.localStorage.setItem("yw_settings","1");
			
			window.localStorage.setItem("yw_breach",yp_info.yw_breach);
			
			window.localStorage.setItem("yp_profile_sync",current_unix_timestamp());
		 
			window.localStorage.setItem("un",email_mobile);
			window.localStorage.setItem("ps",pwd);
			window.localStorage.setItem("yid",yp_info.yp_id);
			
			window.location="home.html";
			
		}
		else{
			$('#validating_div div').html('Login');
			document.login_frm.reset();
			alert("invalid Username/Mobile and Password");
		}

	});
}

function local_validation(email_mobile, pwd){
	var user = window.localStorage.getItem("un");
	var pass = window.localStorage.getItem("ps");
	
	if(user==email_mobile && pwd==pass){
		 var id = window.localStorage.getItem("yid");
		 window.localStorage.setItem("member_id",id);
		 window.location="home.html";
	}
	else{
		$('#validating_div div').html('Login');
		document.login_frm.reset();
		alert("invalid Username/Mobile and Password");
	}
	
}