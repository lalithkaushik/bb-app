
var url="http://lalithkaushik.com/mayot_admin_new/services/";
//var url="http://localhost/mayot_new_theme/public_html/services/";

function logout(){
	window.localStorage.setItem("member_id","");
	//window.plugins.statusBarNotification.clear();
	window.location="index.html";	
}

function notification_show(){
	 navigator.notification.alert(
            'You are in exclusion zone',  // message
            alertDismissed,         // callback
            'MAYOT Alert',            // title
            'OK'                  // buttonName
        );

}

function alertDismissed() {
            // do something
}





function notify_geoloc(){
navigator.geolocation.getCurrentPosition(alertgeo, onError, {enableHighAccuracy: true});
}

function alertgeo(position){
	//alert("My GEO LOC :"+position);
	window.plugins.statusBarNotification.notify("Test", "This is a test notification");
	navigator.notification.beep(2);
	//navigator.notification.vibrate(1000);
}



function review_cooridinates(){
	
	navigator.geolocation.getCurrentPosition(onSuccess, onError, {enableHighAccuracy: true});
	
}

function onSuccess(position) {
	var phoneName =  device.name+','+device.platform+','+device.version;
	var cooridinates = position.coords.latitude+","+position.coords.longitude;
	
	find_breach(position.coords.latitude,position.coords.longitude);
	find_curfew(position.coords.latitude,position.coords.longitude);
	
	var chk_url = url+'push_details.php?cooridinates='+cooridinates+'&device='+escape(phoneName)+'&page=APP';
	$('#show_post').load(chk_url);
}

function onError(error) {
	alert('code: '    + error.code    + '\n' +
		  'message: ' + error.message + '\n');
}

function find_breach(x,y){	

	var fetched = window.localStorage.getArray("yp_exclusion_zones");

	var breach_flag = 0;

	
		for(var each=0;each<fetched.length-1;each++){
			var current_boundaries = fetched[each];
			if(current_boundaries != ""){
			gdata = current_boundaries.split(","); 

			var X_lat='';
			var Y_lng='';
					
			for(i=0;i <= gdata.length-1; ++i){
				if(i%2 !=0)	{	
					
					if(Y_lng==''){
						Y_lng=gdata[i];
					}
					else{
						Y_lng+=','+gdata[i];
					}		
				}
				else{
					if(X_lat==''){
						X_lat=gdata[i];
					}
					else{
						X_lat+=','+gdata[i];
					}	
				}
				 
			}
		}
		
		
				X_lat_split_data = X_lat.split(",");
				Y_lng_split_data = Y_lng.split(",");
				
				
				
				var polySides  = X_lat_split_data.length;
				var polyY = new Array();
				var polyX = new Array();
				
				for(var arrayindex = 0;arrayindex<polySides;arrayindex++){
				polyX[arrayindex] = parseFloat(X_lat_split_data[arrayindex]);
				polyY[arrayindex] = parseFloat(Y_lng_split_data[arrayindex]);
				}
				
				
				var j = polySides-1 ;
				var oddNodes = 0;
				for (i=0; i<polySides; i++){
				if (polyY[i]<y && polyY[j]>=y  ||  polyY[j]<y && polyY[i]>=y){
				if (polyX[i]+(y-polyY[i])/(polyY[j]-polyY[i])*(polyX[j]-polyX[i])<x){
				oddNodes=!oddNodes;
				}
				}
				j=i;
				}
				
				
				if(oddNodes){
				breach_flag = 1;
				
				}
				
				
		
		
		}

		if(breach_flag ==1){
			var coordinates = x+','+y;
			barnotification(coordinates);
		}
		else{
			//window.plugins.statusBarNotification.clear();
		}
	
}

function find_curfew(x,y){	
	
	
	var obj = window.localStorage.getArray("yp_curfew_zone");
	 
	var breach_flag = 0;
	
	if(obj.length>0){
	var curfewtime = obj[0].starttime;
	var time = moment();

	var a = moment(curfewtime, "HH:mm");
	var b = moment(time, "HH:mm");
	
	var c = a.diff(b, 'seconds');
	
	if(c<0){
		var current_boundaries = obj[0].coordinates;
	
		if(current_boundaries.length>2){
			gdata = current_boundaries.split(","); 
		
			var X_lat='';
			var Y_lng='';
					
			for(i=0;i <= gdata.length-1; ++i){
				if(i%2 !=0)	{	
					
					if(Y_lng==''){
						Y_lng=gdata[i];
					}
					else{
						Y_lng+=','+gdata[i];
					}		
				}
				else{
					if(X_lat==''){
						X_lat=gdata[i];
					}
					else{
						X_lat+=','+gdata[i];
					}	
				}
			}
		}
		
		
		X_lat_split_data = X_lat.split(",");
		Y_lng_split_data = Y_lng.split(",");
		
		
		
		var polySides  = X_lat_split_data.length;
		var polyY = new Array();
		var polyX = new Array();
		
		for(var arrayindex = 0;arrayindex<polySides;arrayindex++){
			polyX[arrayindex] = parseFloat(X_lat_split_data[arrayindex]);
			polyY[arrayindex] = parseFloat(Y_lng_split_data[arrayindex]);
		}
		
		
		var j = polySides-1 ;
		var oddNodes = 0;
		for (i=0; i<polySides; i++){
			if (polyY[i]<y && polyY[j]>=y  ||  polyY[j]<y && polyY[i]>=y){
				if (polyX[i]+(y-polyY[i])/(polyY[j]-polyY[i])*(polyX[j]-polyX[i])<x){
					oddNodes=!oddNodes;
				}
			}
			j=i;
		}
		
		
		if(oddNodes){
			breach_flag = 1;
		}


		if(breach_flag==1){
			//window.plugins.statusBarNotification.clear();
		}
		else{
			var coordinates = x+','+y;
			curfew_notification(coordinates);
		}
	}
	}
}

function barnotification(coordinates){

	window.plugins.statusBarNotification.notify("Exclusion", "You are in exclusion zone");
	
	var settings =window.localStorage.getItem("yw_settings");;
	if(settings!="1"){
		navigator.notification.beep(1);
		navigator.notification.vibrate(1000);
	}
	
	var member_id = window.localStorage.getItem("member_id");
	var url1 = url+'breach.php?cooridinates='+coordinates+'&member='+member_id+'&type=1';
	$('#show_post').load(url1);
	
}

function curfew_notification(coordinates){

	window.plugins.statusBarNotification.notify("Curfew", "Time to go home");
	
	var settings =window.localStorage.getItem("yw_settings_curfew");;
	if(settings!="1"){
		navigator.notification.beep(1);
		navigator.notification.vibrate(1000);
	}
	
	var member_id = window.localStorage.getItem("member_id");
	var url1 = url+'breach.php?cooridinates='+coordinates+'&member='+member_id+'&type=2';
	$('#show_post').load(url1);
	
}