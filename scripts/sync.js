//REFRESH TIME IN SECONDS
var refresh_time = 60;

var current_version = '1.4';//14.10.2013

//SERVICES API : Date are supplied from here
var url="http://lalithkaushik.com/mayot_admin_new/services/";

//USE THIS WHILE TESTING IT FROM LOCAL SERVER
//var url="http://localhost/mayot_new_theme/public_html/services/";

//CONVERTING LOCAL STORAGE VARIABLES TO JSON ARRAYS
Storage.prototype.setArray = function(key, obj) {
    return this.setItem(key, JSON.stringify(obj))
}
Storage.prototype.getArray = function(key) {
    return JSON.parse(this.getItem(key))
}


function call_script(){
	var time_interval = refresh_time*1000; //CONVERTING TO MILLISECONDS
	
	
	//ONLY IF LOGGED IN
	var member_id = window.localStorage.getItem("member_id");
	if(member_id!="0" && member_id!=""){
		setTimeout('call_script()',time_interval);
	}
	
		//INITIALISING AN EMPTY ALERT SHOWN ARRAY
		var alerts_shown = window.localStorage.getItem("alert_shown");
		if(alerts_shown==null || alerts_shown==""){

			var valueToPush = new Array();
			valueToPush["text"] = "";
			valueToPush["time"] = "";

			window.localStorage.setArray("alert_shown", valueToPush);
		}
	
	//EXCLUSION ZONE & CURFEW ALERTS
	review_cooridinates();
	
	//IMPORTANTS DATE REMINDER ALERTS
	remind_review();
	
	
	//SYNC FOR EVERY ONE MINUTES EXCLUSION, CURFEW, IMPORTANT DATES
	regular_sync();

}


function current_unix_timestamp(){
	
	var ts = Math.round((new Date()).getTime() / 1000);
	return ts;
}


function regular_sync(){
	var member_id = window.localStorage.getItem("member_id");
	$.getJSON(url+"regular_sync.php", 'member_id='+member_id ,function(response){
		if(response!=""){
			var data = response;
			var excluded_zones = data.exclusion_zones;
			 var split_excluded_coordinates = excluded_zones.split('/');
			 window.localStorage.setArray("yp_map_data", data.map_data);
			 window.localStorage.setArray("yp_curfew_map_data", data.curfew_map_data);
			 window.localStorage.setArray("yp_exclusion_zones", split_excluded_coordinates);	window.localStorage.setItem("yp_exclusion_zones_sync",current_unix_timestamp());
			 window.localStorage.setArray("yp_curfew_zone", data.curfew_zone);					window.localStorage.setItem("yp_curfew_zone_sync",current_unix_timestamp());
			 window.localStorage.setArray("yp_important_dates", data.important_dates);			window.localStorage.setItem("yp_important_dates_sync",current_unix_timestamp());
			 window.localStorage.setItem("yw_breach",data.yw_breach);
		}
	});
}


function sync(page){

	var member_id = window.localStorage.getItem("member_id");
	
	$.getJSON(url+"sync.php", 'member_id='+member_id ,function(response){
		if(response!=""){
			var data = response;
			var excluded_zones = data.exclusion_zones;
			 var split_excluded_coordinates = excluded_zones.split('/');
			 window.localStorage.setArray("yp_map_data", data.map_data);
			 window.localStorage.setArray("yp_curfew_map_data", data.curfew_map_data);
			 window.localStorage.setArray("yp_exclusion_zones", split_excluded_coordinates);	window.localStorage.setItem("yp_exclusion_zones_sync",current_unix_timestamp());
			 window.localStorage.setArray("yp_curfew_zone", data.curfew_zone);					window.localStorage.setItem("yp_curfew_zone_sync",current_unix_timestamp());
			 window.localStorage.setArray("yp_activities", data.activities);					window.localStorage.setItem("yp_activities_sync",current_unix_timestamp());
			 window.localStorage.setArray("yp_activities_progress", data.activities_progress);	window.localStorage.setItem("yp_activities_progress_sync",current_unix_timestamp());
			 window.localStorage.setArray("yp_important_dates", data.important_dates);			window.localStorage.setItem("yp_important_dates_sync",current_unix_timestamp());
			 window.localStorage.setArray("yp_useful_info", data.useful_info);					window.localStorage.setItem("yp_useful_info_sync",current_unix_timestamp());
			 window.localStorage.setArray("yp_contact_info", data.contact_info);				window.localStorage.setItem("yp_contact_info_sync",current_unix_timestamp());
			 window.localStorage.setItem("yw_breach",data.yw_breach);
			
			 window.location=page+".html";
		}
	});
}


function remind_review(){
	var reviews = window.localStorage.getArray("yp_important_dates");
	var hours ;
	var text ;
	var a;
	var b;
	var t;

	if(reviews.length!=0){
		$.each(reviews, function(index, review) {
			t = current_unix_timestamp();
			
			a = moment.unix(review.date);
			b = moment.unix(t);
			hours = a.diff(b, 'hours');
			
			if(hours>0 && hours<=24){
				
				text = "Reminder : "+review.description+' '+moment.unix(review.date).format('dddd, Do MMMM @ hh:mm a');
				show_reminder_reviews(text);

			}
			
		});

	}
}




function show_reminder_reviews(text){
	

	if(alert_displayed_before(text)>=24 || alert_displayed_before(text)==0){

		window.plugins.statusBarNotification.notify("Mayot", text);
		navigator.notification.beep(2);
		navigator.notification.vibrate(1000);
		var alerts_shown = window.localStorage.getArray("alert_shown");
		
		
		var valueToPush = new Object();
		valueToPush["text"] = text;
		valueToPush["time"] = current_unix_timestamp();
		alerts_shown.push(valueToPush);
		window.localStorage.setArray("alert_shown", alerts_shown);
	}
		
		
}

function alert_displayed_before(text){
	
	
	var alerts_shown = window.localStorage.getArray("alert_shown");
	var reminds = alerts_shown;
	var duration = 0;
	var a;
	var b;
	var d;
	var t = current_unix_timestamp();
	if(alerts_shown!=null){
		if(alerts_shown.length!=0){
			$.each(reminds, function(index, remind) {
				console.log('duration check '+remind.text);
				if(remind.text==text){
					console.log('duration check find '+remind.time+' - '+t);
					a = moment.unix(remind.time);
					b = moment.unix(t);
					d = b.diff(a, 'hours', true);
					console.log('duration value '+d);
					duration = d;
				}
			});
		}
	}
	console.log('duration '+duration);
	return duration;
}